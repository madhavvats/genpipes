//Width of the window is set depending on the flowchart width.
var width = $('#{{last}}').offset()['left']+$('#{{last}}').width()+30;
var height = window.innerHeight;

//Konva Stage in created.
var stage = new Konva.Stage({
    container: 'container',
    width: width,
    height: height
});

//Konva layer is created
var layer = new Konva.Layer();

// function to determine the center of the arc given two points and the angle of the arc on the center.
function center(x1,y1,x2,y2,angle) {
    var chord_length = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
    var radius = (chord_length / 2) / Math.cos(angle);
    var array = [0,0,0];
    array[0] = x1 + radius*Math.cos(angle);
    array[1] = y1 + radius*Math.sin(angle);
    array[2] = radius;
    return array;
}

//Jinja loop to create the arcs between the steps and their dependencies with same angle at 60 degrees.
{% for step, value in parent.items() -%}
{% for inner_step, value2 in value.items() -%}
var start = $('#{{step}}').offset();
var end = $('#{{inner_step}}').offset();
var arc_req = center(start['left'] + $('#{{step}}').width() / 2, start['top'], end['left'] + $('#{{inner_step}}').width() / 2, end['top'], Math.PI / 3);
var {{step}}{{inner_step}} = new Konva.Arc({ x: arc_req[0], y: arc_req[1], innerRadius: arc_req[2], outerRadius: arc_req[2], angle: 60, rotationDeg: -120, stroke: 'black', strokeWidth: 2.2 });
layer.add({{step}}{{inner_step}});
var {{step}}{{inner_step}}triangle = new Konva.RegularPolygon({ x: end['left'] - 3.5 + $('#{{inner_step}}').width() / 2, y: end['top'] - 2.5, sides: 3, radius: 1, fill: 'black', stroke: 'black', strokeWidth: 4 });
layer.add({{step}}{{inner_step}}triangle);
{%- endfor %}
{%- endfor %}
stage.add(layer);

//Jinja loops to add the hover functionality between the steps and their dependencies.
{% for step, value in data.items() -%}
$("#{{step}}").hover(function () {
{% for inner_step in value.parent -%}
{{step}}{{inner_step}}.stroke('green');
{{step}}{{inner_step}}triangle.stroke('green');
layer.draw();
{%- endfor %}
{% for child in value.children -%}
{{child}}{{step}}.stroke('blue');
{{child}}{{step}}triangle.stroke('blue');
layer.draw();
{%- endfor %}
}, function () {
{% for inner_step in value.parent -%}
{{step}}{{inner_step}}.stroke('black');
{{step}}{{inner_step}}triangle.stroke('black');
layer.draw();
{%- endfor %}
{% for child in value.children -%}
{{child}}{{step}}.stroke('black');
{{child}}{{step}}triangle.stroke('black');
layer.draw();
{%- endfor %}
});
{%- endfor %}

//Jinja loops to add the information about the input/output files.
{% for step in flow_list -%}
{% if step.input_file_type|string()!='' %}
var img = document.createElement("img");
img.src = "https://i.ibb.co/MstwQwC/file-02.png";
img.style.position = 'absolute';
var num = $('#{{step.step_name}}').offset()['left'] + $('#{{step.step_name}}').width()/2-21/2;
img.style.left = num.toString()+"px";
img.style.width = "21px";
img.style.width = "28px";
img.style.top = '280px';
document.getElementById('io').appendChild(img);
var para = document.createElement("P");
para.style.position = 'absolute';
para.style.left = num.toString() + "px";
num=num-6;
para.style.width = '29px';
para.style.fontSize = '7px';
para.style.textAlign = 'center';
para.innerText = "{{ step.input_file_type }}";
para.style.fontWeight = 'bold';
para.style.top = '320px';
document.getElementById('io').appendChild(para);
var arrow = new Konva.Arrow({
    points: [$('#{{step.step_name}}').offset()['left'] + $('#{{step.step_name}}').width() / 2, 280, $('#{{step.step_name}}').offset()['left'] + $('#{{step.step_name}}').width() / 2, $('#{{step.step_name}}').offset()['top'] + 2 * $('#{{step.step_name}}').height() + 4],
    pointerLength: 5,
    pointerWidth: 5,
    fill: 'black',
    stroke: 'black',
    strokeWidth: 1.5
});
layer.add(arrow);
layer.draw();
{% endif %}
{% if step.output_file_type | string()!='' %}
var img = document.createElement("img");
img.src = "https://i.ibb.co/f4WLJNc/file-01.png";
img.style.position = 'absolute';
var num = $('#{{step.step_name}}').offset()['left'] + $('#{{step.step_name}}').width()/2-21/2;
img.style.left = num.toString() + "px";
img.style.width = "21px";
img.style.width = "28px";
img.style.top = '280px';
document.getElementById('io').appendChild(img);
var para = document.createElement("P");
para.style.position = 'absolute';
para.style.left = num.toString() + "px";
num=num-6;
para.style.width = '29px';
para.style.fontSize = '7px';
para.style.textAlign = 'center';
para.innerText = "{{ step.output_file_type }}";
para.style.fontWeight = 'bold';
para.style.top = '320px';
document.getElementById('io').appendChild(para);
var arrow = new Konva.Arrow({
    points: [$('#{{step.step_name}}').offset()['left'] + $('#{{step.step_name}}').width() / 2, $('#{{step.step_name}}').offset()['top'] + 2 * $('#{{step.step_name}}').height(), $('#{{step.step_name}}').offset()['left'] + $('#{{step.step_name}}').width() / 2, 278],
    pointerLength: 5,
    pointerWidth: 5,
    fill: 'black',
    stroke: 'black',
    strokeWidth: 1.5
});
layer.add(arrow);
layer.draw();
{% endif %}
{%- endfor %}
var img1;

//Flowchart is converted to a canvas object using html2canvas.
html2canvas(document.getElementById('wrap'), {
    allowTaint: true, useCORS: true, width: $('#{{last}}').offset()['left'] + $('#{{last}}').width() + 30,
    height: 530,
}).then(canvas => {
    img1 = canvas.toDataURL();
});

//function to save the image.
function save() {
    console.log(img1);
    download(img1, 'flowchart.png');
}

//function to download the canvas object as PNG image.
function download(im, filename) {
    var lnk = document.createElement('a'), e;
    lnk.download = filename;
    lnk.href = im;
    if (document.createEvent) {
        e = document.createEvent("MouseEvents");
        e.initMouseEvent("click", true, true, window,
            0, 0, 0, 0, 0, false, false, false,
            false, 0, null);

        lnk.dispatchEvent(e);
    } else if (lnk.fireEvent) {
        lnk.fireEvent("onclick");
    }
}
