""" This python module contains the function make_flowchart which takes in the parameters:-flow_list,
    about_stepstorage which are provided from core/pipeline. Two dictionaries parent, children, are created 
    from this info containing steps and their dependencies and vice versa. This data is then used to render 
    the templates."""
from jinja2 import Environment, FileSystemLoader
import sys
import logging
import os
import shutil
loc = os.path.dirname(os.path.realpath(sys.argv[0]))
#Function to create the Flowchart.
def make_flowchart(flow_list,about_step,storage):
    try:
        os.mkdir(storage+"/flowchart")
    except:
        pass
    log = logging.getLogger(__name__)
    for data in flow_list:
        try:
            data["about"] = str('"')+about_step[str(data["step_name"])]+str('"')
        except:
            pass
    file_loader = FileSystemLoader(loc[:loc[:loc.rfind("/")].rfind("/")]+'/Flowchart/templates')
    env = Environment(loader=file_loader,extensions=['jinja2.ext.loopcontrols'])
    template = env.get_template('index.html')
    template2 = env.get_template('canvas_template.js')
    #Steps are mapped with their dependecies.
    parent = {}
    for step in flow_list:
        parent[step['step_name']] = {}
        for inner_step in flow_list:
            for dependency in inner_step['job_dependencies']:
                for job in step['job_name']:
                    if job==dependency:
                        parent[step['step_name']][inner_step['step_name']]=1
    #Dependencies are mapped with their parent steps
    children = {}
    for step in parent:
        children[step] = []
    for step in parent:
        for child in parent[step]:
            children[child].append(step)
    data = {}
    for step in parent:
        data[step] = {'parent':[],'children':[]}
    for step in parent:
        for p in parent[step]:
            data[step]['parent'].append(p)
        data[step]['children']=children[step]
    #canvas_template template is rendered and a file named canvas.js is created.
    output_canvas = template2.render(data=data,parent=parent,flow_list=flow_list,last=flow_list[-1]["step_name"])
    f = open(storage+"/flowchart/canvas.js", "w+")
    f.write(output_canvas)
    f.close()
    #index template is rendered and a file named flow_html.html is created.
    output = template.render(data=flow_list)
    f2 = open(storage+"/flowchart/flow_html.html", "w+")
    f2.write(output)
    f2.close()
    #style.css is copied to the output location.
    shutil.copyfile(loc[:loc[:loc.rfind("/")].rfind("/")]+"/Flowchart/style.css", storage+"/flowchart/style.css")
    #html2canvas.js is copied to the output location.
    shutil.copyfile(loc[:loc[:loc.rfind("/")].rfind("/")]+"/Flowchart/html2canvas.js", storage+"/flowchart/html2canvas.js")